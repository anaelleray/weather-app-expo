import React from 'react';
import {Provider} from "react-redux"; 
import store from './src/store';
import { ImageBackground, StyleSheet, View } from 'react-native';
import WeatherComponent from './src/Component/WeatherComponent';

export default function App() {
  return (
    <Provider store={store}>
      <View style={styles.container}>
        <ImageBackground style={styles.imageBackground} source={require('./assets/background.png')}>
          <WeatherComponent></WeatherComponent>
        </ImageBackground>
      </View>
    </Provider>

  );
}
const styles = StyleSheet.create({
  container: {
  },
  imageBackground: {
    height: 1000
  }
});
