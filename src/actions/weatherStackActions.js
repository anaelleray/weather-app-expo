/**
 * Cette fonction va avoir pour but de prendre en compte la valuer du Select des villes pour ajouter un paramètre à l'objet "weather" qui selectedCity et c'est cette valeur qui va être utilsée dans la fonction du dessous pour adapter l'url de wetaherStack avec la query de la bonne ville correspondante.
 */
export const onCityChangeHandler = (event) => {
    return (dispatch) => {
        dispatch({
            type: "SELECTED_CITY",
            value: event.value
        })
    }
}
/*-------------------------------------------*/

/**
 * Ici on va aller chercher les states de weather et vérifier si son state "selectedCity" renvoit quelque chose (voir fonction du dessus) et en fonction de ça, si ce n'est pas null, on va utiliser la donnée (le nom d'une ville en string) pour changer directement l'url du fetch() avec cette dit data et ensuite réecrire l'object weather avec les données de la ville correspondante et pouvoir utiliser ça pour mettre à jour la vue.
 */
export const fetchInfos = () => {
    return async (dispatch, getState) => {

        /*--------------------BASE KEY-----------------------*/
        // const response = await fetch("http://api.weatherstack.com/current?access_key=5b53e7944332d2fdb3445e7b0bbb14bc&query=Toulouse");
        /*--------------------BASE KEY-----------------------*/

        if (getState().weather.selectedCity !== null) {

            let cityState = getState().weather.selectedCity

            let newUrl = `http://api.weatherstack.com/current?access_key=5b53e7944332d2fdb3445e7b0bbb14bc&query=${cityState}`

            const response = await fetch(newUrl);
            let weatherData = await response.json();

            if (weatherData !== null && weatherData !== undefined) {

                let weather = {
                    Name: weatherData.location.name,
                    Date: weatherData.location.localtime,
                    WeatherDesc: weatherData.current.weather_descriptions[0],
                    Temperature: weatherData.current.temperature,
                    Wind: weatherData.current.wind_speed,
                    Humidity: weatherData.current.humidity,
                    selectedCity: null
                }
                dispatch({
                    type: "SET_DATA_FROM_CITY",
                    payload: weather
                });
            } else {
                console.log('Error data fetch');
            }
        } else {
            // console.log('Error selectedCity');
        }
    }
}
/*------------------------------------------*/


