import React, { Component } from "react";
import { Image, Text, View, StyleSheet } from "react-native";
import { connect } from "react-redux";
import DropDownPicker from 'react-native-dropdown-picker';
import { fetchInfos, onCityChangeHandler } from '../actions/weatherStackActions'
import Loader from "../Loader";

class WeatherComponent extends Component {
    newDate = () => {
        let dateMoment = moment(this.props.weather.Date).format("dd mmmm yyyy")
        console.log(dateMoment);
    }
    render() {
        if (!this.props.weather) {
            return (
                <Loader />
            )
        }
        { this.props.fetchInfos() }

        return (
            <View style={styles.mainView}>
                <View style={styles.titleView}>
                    <Text style={styles.title}>{this.props.weather.Name}</Text>
                    <Text style={styles.date}>{this.props.weather.Date}</Text>
                </View>
                <View style={styles.contWhite}>
                    <View style={{ flexDirection: "row", marginBottom: 10 }}>
                        <Image source={require('../../assets/weather.png')}></Image>
                        <Text style={styles.weatherDesc}>{this.props.weather.WeatherDesc}</Text>
                    </View>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={styles.temperature}>{this.props.weather.Temperature}°C</Text>
                        <View>
                            <Text style={styles.minMax}>Min -146°C</Text>
                            {/* <hr></hr> */}
                            <Text style={styles.minMax}>Max +84°C</Text>
                        </View>

                    </View>
                </View>
                <View style={styles.contPurple}>
                    <View style={{ flexDirection: "row", alignItems: "baseline" }}>
                        <Image source={require('../../assets/wind.png')}></Image>
                        <Text style={styles.windhumText}>{this.props.weather.Wind} Km/h</Text>
                        <Image source={require('../../assets/double_drop.png')} style={{ resizeMode: "stretch", height: 50, width: 50 }}></Image>
                        <Text style={styles.windhumText}>{this.props.weather.Humidity} %</Text>
                    </View>
                </View>

                <View>
                    <DropDownPicker
                        items={[
                            { label: 'Lyon', value: 'Lyon' },
                            { label: 'Paris', value: 'Paris' },
                            { label: 'Toulouse', value: 'Toulouse' },
                            { label: 'Marseille', value: 'Marseille' },
                            { label: 'Londres', value: 'Londres' },
                            { label: 'Berlin', value: 'Berlin' },
                            { label: 'Dublin', value: 'Dublin' },
                            { label: 'Galway', value: 'Galway' },
                            { label: 'Washington', value: 'Washington' },
                            { label: 'Budapest', value: 'Budapest' },
                            { label: 'Moscou', value: 'Moscou' },
                            { label: 'Tokyo', value: 'Tokyo' },
                            { label: 'Seoul', value: 'Seoul' },
                        ]}
                        defaultValue={"Lyon"}
                        containerStyle={{ height: 40, width: 200, alignSelf: "center", opacity:0.75, }}
                        style={{ backgroundColor: '#fafafa' }}
                        labelStyle={{
                            fontSize: 20,
                            textAlign: 'center',
                            color: 'rgb(65, 70, 143)',
                            fontWeight: "bold"
                        }}
                        dropDownStyle={{ backgroundColor: '#fafafa' }}
                        onChangeItem={this.props.onCityChangeHandler}
                    />
                </View>
            </View>
        )
    }
}
/*----------STYLE BLOC-------------------*/

const styles = StyleSheet.create({
    mainView: {
        display: 'flex',
        marginTop: 100,
        alignContent: "center",
        alignSelf: "center",
        flexDirection: 'column',
        justifyContent: 'center'
    },
    titleView: {
        alignItems: "center",
        marginBottom: 50
    },
    title: {
        fontSize: 50,
        color: 'white',
        fontWeight: "bold"
    },
    date: {
        fontSize: 30,
        color: 'white',
        fontWeight: "bold"
    },
    contWhite: {
        backgroundColor: "white",
        borderRadius: 20,
        opacity:0.75,
        padding: 20,
        height: 230,
        flexWrap: "wrap"
    },
    weatherDesc: {
        fontSize: 30,
        color: 'rgb(65, 70, 143)',
        fontWeight: "bold",
        marginLeft: 20,
        marginTop: 20
    },
    temperature: {
        fontSize: 60,
        color: 'rgb(65, 70, 143)',
        fontWeight: "bold"
    },
    minMax: {
        fontSize: 15,
        color: '#444444',
        fontWeight: "bold",
        marginLeft: 10,
        justifyContent: "center",
        marginTop: 10
    },
    contPurple: {
        backgroundColor: 'rgb(65, 70, 143)',
        borderRadius: 20,
        padding: 10,
        width: 240,
        marginBottom: 30,
        marginTop: -30,
        alignSelf: "center",
    },
    windhumText: {
        color: "white",
        fontWeight: "bold",
        fontSize: 15,
        padding: 5
    },
})

/*---------------------------------------*/
const mapStateToProps = state => {
    return {
        weather: state.weather
    }
}
const mapDispatchToProps = (dispatch) => {
    return {
        fetchInfos: () => {
            dispatch(fetchInfos())
        },
        onCityChangeHandler: (event) => {
            dispatch(onCityChangeHandler(event))
        }
    }
}
export default connect(mapStateToProps, mapDispatchToProps)(WeatherComponent)