import React from 'react'

const Loader = () => (
    <div className="loader">
    <div className="center-all">
        <i className="fa fa-spin fa-spinner"></i>
    </div>
    </div>
)

export default Loader