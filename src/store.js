import { createStore, applyMiddleware } from 'redux';
import { composeWithDevTools } from 'redux-devtools-extension';
import thunk from 'redux-thunk';
import weatherStackReducer from './reducers/weatherStackReducer'

const composeEnhancers = composeWithDevTools({});

const middleware = [
    thunk
];

export default createStore(
    weatherStackReducer,
    composeEnhancers(applyMiddleware(...middleware))

);